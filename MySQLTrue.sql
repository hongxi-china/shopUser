-- auto-generated definition
create table userinfo
(
    UID           int auto_increment comment '用户ID'
        primary key,
    username      char(255)   not null comment '账号',
    password      text        not null comment '密码',
    img           char(255)   null comment '头像',
    nikename      char(255)   null comment '昵称',
    job           char(255)   null comment '工作',
    hobby         char(255)   null comment '爱好',
    register_time datetime(6) not null comment '注册时间',
    birthday      date        null comment '生日',
    IDcard        int         null comment '身份证',
    signature     char(255)   null comment '个性签名',
    constellation char(255)   null comment '星座',
    IsGroom       tinyint(1)  null,
    Credits       int         null,
    mail          char(255)   null,
    phone         bigint      null,
    constraint username
        unique (username)
)
    comment '用户信息';

-- auto-generated definition
create table cart
(
    username char(255) null,
    ID       int       null
);

-- auto-generated definition
create table chat
(
    user1     char(255) null,
    user2     char(255) null,
    message1  longtext  null,
    message2  longtext  null,
    chat_time datetime  null
);

-- auto-generated definition
create table collection
(
    username char(255) null,
    ID       int       null
);

-- auto-generated definition
create table mail_code
(
    mail      char(255) null,
    mail_code int       null,
    date_time datetime  null
);

-- auto-generated definition
create table productcategories
(
    CategoryID       int auto_increment
        primary key,
    CategoryName     varchar(255)                        not null,
    ParentID         int                                 null,
    Description      text                                null,
    CreationDate     timestamp default CURRENT_TIMESTAMP null,
    LastModifiedDate timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
);

-- auto-generated definition
create table products
(
    ProductID         int auto_increment comment '商品ID'
        primary key,
    ProductName       varchar(255)                                                                                                                                                                          null comment '商品名称',
    OwnerID           int                                                                                                                                                                                   null comment '商品所属人ID',
    Price             decimal(10, 2)                                                                                                                                                                        null comment '商品价格',
    ImageURL          longtext                                                                                                                                                                              null comment '商品图片URL',
    Description       text                                                                                                                                                                                  null comment '商品描述',
    `Condition`       enum ('全新', '九成新', '八成新', '七成新', '六成新', '五成新', '损坏')                                                                                                               null comment '商品状态',
    Category          enum ('家具', '电子产品', '书籍', '时尚服饰', '奢侈品', '美妆个护', '家用电器', '玩具与游戏', '运动健身器材', '音乐与影视', '艺术与收藏品', '手工艺品', '办公设备与文具', '汽车配件') null comment '商品类别',
    PostedDate        date                                                                                                                                                                                  null comment '发布日期',
    IsSold            tinyint(1)                                                                                                                                                                            null comment '是否已售',
    IsGroom           tinyint(1)                                                                                                                                                                            null,
    Image_two         longtext                                                                                                                                                                              null,
    image_three       longtext                                                                                                                                                                              null,
    image_four        longtext                                                                                                                                                                              null,
    image_five        longtext                                                                                                                                                                              null,
    image_six         longtext                                                                                                                                                                              null,
    who_buy           char(255)                                                                                                                                                                             null,
    success_shipments tinyint(1)                                                                                                                                                                            null,
    constraint products_ibfk_1
        foreign key (OwnerID) references userinfo (UID)
)
    comment '二手交易市场的商品表';

create index OwnerID
    on products (OwnerID);

-- auto-generated definition
create table token
(
    username varchar(255) not null,
    token    longtext     not null
);


-- auto-generated definition
create table yesapi_xbinstore_tb_index_slide_ad
(
    id                                        bigint unsigned auto_increment
        primary key,
    alt                                       varchar(5)    null comment '提示',
    clog                                      varchar(100)  null comment 'log号',
    ext1                                      varchar(5)    null comment '无',
    href                                      varchar(500)  null comment '网址',
    logV                                      varchar(5)    null comment 'log等级',
    src                                       longtext      null comment '大图片',
    srcB                                      varchar(200)  null comment '小图片',
    yesapi_xbinstore_tb_index_slide_ad_status int default 1 null comment '状态。可选值:1(正常),2(删除)',
    sort_order                                int default 1 null comment '排序',
    logDomain                                 varchar(200)  null comment 'log记录网址前缀',
    created                                   datetime      null,
    updated                                   datetime      null
)
    comment 'B2C商城-首页轮播广告';

-- 插入父类别数据
INSERT INTO ProductCategories (CategoryName) VALUES ('家具'); -- 插入家具类别
INSERT INTO ProductCategories (CategoryName) VALUES ('电子产品'); -- 插入电子产品类别
INSERT INTO ProductCategories (CategoryName) VALUES ('书籍'); -- 插入书籍类别
INSERT INTO ProductCategories (CategoryName) VALUES ('时尚服饰'); -- 插入时尚服饰类别
INSERT INTO ProductCategories (CategoryName) VALUES ('奢侈品'); -- 插入奢侈品类别
INSERT INTO ProductCategories (CategoryName) VALUES ('美妆个护'); -- 插入美妆个护类别
INSERT INTO ProductCategories (CategoryName) VALUES ('家用电器'); -- 插入家用电器类别
INSERT INTO ProductCategories (CategoryName) VALUES ('玩具与游戏'); -- 插入玩具与游戏类别
INSERT INTO ProductCategories (CategoryName) VALUES ('运动健身器材'); -- 插入运动健身器材类别
INSERT INTO ProductCategories (CategoryName) VALUES ('音乐与影视'); -- 插入音乐与影视类别
INSERT INTO ProductCategories (CategoryName) VALUES ('艺术与收藏品'); -- 插入艺术与收藏品类别
INSERT INTO ProductCategories (CategoryName) VALUES ('手工艺品'); -- 插入手工艺品类别
INSERT INTO ProductCategories (CategoryName) VALUES ('办公设备与文具'); -- 插入办公设备与文具类别
INSERT INTO ProductCategories (CategoryName) VALUES ('汽车配件'); -- 插入汽车配件类别