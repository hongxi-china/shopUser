
    function checkCredentials() {
        var username = localStorage.getItem('username');
        var token = localStorage.getItem('token');

        if (username && token) {
            fetch('/api/check_token/fdgasfghjsadfjasifguasncsadfgsayuifcsdabvsadfbfsadvjckav?username=' + encodeURIComponent(username) + '&token=' + encodeURIComponent(token))
                .then(response => response.json())
                .then(data => {
                    if (data.data.code !== 200) {
                        // Token验证失败，清除localStorage中的username和token，然后重定向到登录页面
                        localStorage.removeItem('username');
                        localStorage.removeItem('token');
                        localStorage.removeItem('jwtToken');
                        document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                        document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                        alert("登录过期，请重新登录！");
                        window.location.href = "/login";
                    }
                })
                .catch(error => console.error('错误:', error));
        } else {
            // 没有username或token，重定向到登录页面
            alert("登录过期，请重新登录！");
            window.location.href = "/login";
        }
    }

    // 每秒钟检查一次
    setInterval(checkCredentials, 5000);
