document.getElementById('sendSmsCode').addEventListener('click', function (event) {
    event.preventDefault();
    var mail = document.getElementById('mail').value;
    fetch('/api/send_mail_code', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            mail: mail
        })
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        if (data.data.code === 200) {
            alert('验证码发送成功');
            // 禁用发送验证码按钮
            var btn = document.getElementById('sendSmsCode');
            btn.disabled = true;
            // 开始倒计时
            var countdown = 60;
            var timer = setInterval(function () {
                countdown--;
                btn.textContent = countdown + '秒后重新发送';
                if (countdown <= 0) {
                    // 倒计时结束，重新启用发送验证码按钮
                    clearInterval(timer);
                    btn.textContent = '发送邮箱验证码';
                    btn.disabled = false;
                }
            }, 1000);
        } else {
            alert('验证码发送失败: ' + data.data.msg);
        }
    });
});

var usernameInput = document.getElementById('username');
var registerBtn = document.getElementById('register');

usernameInput.addEventListener('input', function (event) {
    var username = usernameInput.value;
    if (username.trim() === '') {
        // 用户名输入框为空，不发送请求
        registerBtn.disabled = true;
    } else {
        fetch('/api/check_username', {  // 假设您有一个检查用户名的API端点
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username
            })
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data.data.code === 400) {
                // 用户名已被注册，显示提示信息并禁用注册按钮
                alert('用户名已被注册，请更换用户名');
                registerBtn.disabled = true;
            } else {
                // 用户名未被注册，启用注册按钮
                registerBtn.disabled = false;
            }
        });
    }
});
var mailInput = document.getElementById('mail');
var sendSmsCodeBtn = document.getElementById('sendSmsCode');
mailInput.addEventListener('input', function (event) {
    var mail = mailInput.value;
    if (mail.trim() === '') {
        // 邮箱输入框为空，不发送请求
        sendSmsCodeBtn.disabled = true;
    } else {
        fetch('/api/check_mail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                mail: mail
            })
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data.data.code === 400) {
                // 邮箱已被注册，显示提示信息并禁用发送验证码按钮
                alert('邮箱已被注册，请更换邮箱');
                sendSmsCodeBtn.disabled = true;
            } else {
                // 邮箱未被注册，启用发送验证码按钮
                sendSmsCodeBtn.disabled = false;
            }
        });
    }
});

document.getElementById('register').addEventListener('click', function (event) {
    event.preventDefault();
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    var mail = document.getElementById('mail').value;
    var phone = document.getElementById('phone').value;
    var mail_code = document.getElementById('smsCode').value;

    // 先进行https://chat.boqikc.com的注册请求
    fetch('https://chat.boqikc.com/api/user/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': '*/*',
            'Origin': 'https://chat.boqikc.com'
        },
        body: JSON.stringify({
            name: username,
            email: mail,
            password: password,
            gender: 0,
            device: 'browser'
        })
    }).then(function (response) {
        if (response.status === 200) {
            // 如果https://chat.boqikc.com的注册请求成功，进行/api/logon请求
            return response.json();
        } else {
            throw new Error('注册失败: ' + response.statusText);
        }
    }).then(function (data) {
        fetch('/api/logon', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password,
                mail: mail,
                phone: phone,
                mail_code: mail_code
            })
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data.data.code === 200) {
                alert('注册成功');
                location.href = '/login';
            } else {
                alert('注册失败: ' + data.data.msg);
            }
        });
    }).catch(function (error) {
        alert(error);
    });
});