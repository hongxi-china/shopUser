CREATE TABLE `shop`.`userinfo`
(
    `UID`           INT         NOT NULL AUTO_INCREMENT,
    `username`      CHAR(255)   NOT NULL UNIQUE,
    `password`      TEXT        NOT NULL,
    `img`           CHAR(255),
    `nikename`      CHAR(255),
    `job`           CHAR(255),
    `hobby`         CHAR(255),
    `register_time` DATETIME(6) NOT NULL,
    `birthday`      DATE,
    `IDcard`        INT,
    `signature`     CHAR(255),
    `constellation` CHAR(255),
    IsGroom         BOOLEAN COMMENT '是否推荐',
    Credits         INT COMMENT '信用分',
    PRIMARY KEY (`UID`)
);

ALTER TABLE `shop`.`userinfo`
    COMMENT = '用户信息';

ALTER TABLE `shop`.`userinfo`
    CHANGE `UID` `UID` INT NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    CHANGE `username` `username` CHAR(255) NOT NULL COMMENT '账号',
    CHANGE `password` `password` TEXT NOT NULL COMMENT '密码',
    CHANGE `img` `img` CHAR(255) COMMENT '头像',
    CHANGE `nikename` `nikename` CHAR(255) COMMENT '昵称',
    CHANGE `job` `job` CHAR(255) COMMENT '工作',
    CHANGE `hobby` `hobby` CHAR(255) COMMENT '爱好',
    CHANGE `register_time` `register_time` DATETIME(6) NOT NULL COMMENT '注册时间',
    CHANGE `birthday` `birthday` DATE COMMENT '生日',
    CHANGE `IDcard` `IDcard` INT COMMENT '身份证',
    CHANGE `signature` `signature` CHAR(255) COMMENT '个性签名',
    CHANGE `constellation` `constellation` CHAR(255) COMMENT '星座';

CREATE TABLE `token` (
    `username` VARCHAR(255) NOT NULL,
    `token` LONGTEXT NOT NULL
);
DELIMITER $$
CREATE EVENT `ev_clear_token`
ON SCHEDULE EVERY 7 DAY
DO
BEGIN
    DELETE FROM `token`;
END $$
DELIMITER ;
SET GLOBAL event_scheduler = ON;SET GLOBAL event_scheduler = ON;

SELECT * FROM information_schema.EVENTS WHERE EVENT_NAME = 'ev_clear_token';
ALTER EVENT ev_clear_token ENABLE;
# 轮播图
CREATE TABLE `yesapi_xbinstore_tb_index_slide_ad`
(
    `id`                                        bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `alt`                                       varchar(5)          NULL COMMENT '提示',
    `clog`                                      varchar(100)        NULL COMMENT 'log号',
    `ext1`                                      varchar(5)          NULL COMMENT '无',
    `href`                                      varchar(500)        NULL COMMENT '网址',
    `logV`                                      varchar(5)          NULL COMMENT 'log等级',
    `src`                                       LONGTEXT            NULL COMMENT '大图片',
    `srcB`                                      varchar(200)        NULL COMMENT '小图片',
    `yesapi_xbinstore_tb_index_slide_ad_status` int(1)              NULL DEFAULT '1' COMMENT '状态。可选值:1(正常),2(删除)',
    `sort_order`                                int(4)              NULL DEFAULT '1' COMMENT '排序',
    `logDomain`                                 varchar(200)        NULL COMMENT 'log记录网址前缀',
    `created`                                   datetime            NULL COMMENT '',
    `updated`                                   datetime            NULL COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB COMMENT 'B2C商城-首页轮播广告';


INSERT INTO `yesapi_xbinstore_tb_index_slide_ad`
(`alt`, `clog`, `ext1`, `href`, `logV`, `src`, `srcB`, `yesapi_xbinstore_tb_index_slide_ad_status`, `sort_order`,
 `logDomain`, `created`, `updated`)
VALUES ('首页', 'C1234', '扩展', 'https://www.example.com', '高', 'https://www.example.com/images/large.jpg',
        'https://www.example.com/images/small.jpg', 1, 1, 'https://log.example.com', NOW(), NOW());

INSERT INTO `yesapi_xbinstore_tb_index_slide_ad`
(`alt`, `clog`, `ext1`, `href`, `logV`, `src`, `srcB`, `yesapi_xbinstore_tb_index_slide_ad_status`, `sort_order`,
 `logDomain`, `created`, `updated`)
VALUES ('关于', 'C5678', '信息', 'https://www.about.com', '中', 'https://www.about.com/images/large.jpg',
        'https://www.about.com/images/small.jpg', 1, 2, 'https://log.about.com', NOW(), NOW());

INSERT INTO `yesapi_xbinstore_tb_index_slide_ad`
(`alt`, `clog`, `ext1`, `href`, `logV`, `src`, `srcB`, `yesapi_xbinstore_tb_index_slide_ad_status`, `sort_order`,
 `logDomain`, `created`, `updated`)
VALUES ('服务', 'C91011', '详情', 'https://www.services.com', '低', 'https://www.services.com/images/large.jpg',
        'https://www.services.com/images/small.jpg', 1, 3, 'https://log.services.com', NOW(), NOW());


# 商品表
CREATE TABLE Products
(
    ProductID   INT AUTO_INCREMENT COMMENT '商品ID',
    ProductName VARCHAR(255) COMMENT '商品名称',
    OwnerID     INT COMMENT '商品所属人ID',
    Price       DECIMAL(10, 2) COMMENT '商品价格',
    ImageURL    Longtext COMMENT '商品图片URL',
    Description TEXT COMMENT '商品描述',
    `Condition` ENUM('全新', '九成新', '八成新', '七成新', '六成新', '五成新', '损坏') COMMENT '商品状态',
    Category    ENUM('家具', '电子产品', '书籍', '时尚服饰', '奢侈品', '美妆个护', '家用电器', '玩具与游戏', '运动健身器材', '音乐与影视', '艺术与收藏品', '手工艺品', '办公设备与文具', '汽车配件') COMMENT '商品类别',
    PostedDate  DATE COMMENT '发布日期',
    IsSold      BOOLEAN COMMENT '是否已售',
    IsGroom     BOOLEAN COMMENT '是否推荐',
    image_two   Longtext COMMENT '商品图片URL2',
    image_three Longtext COMMENT '商品图片URL3',
    image_four  Longtext COMMENT '商品图片URL4',
    image_five  Longtext COMMENT '商品图片URL5',
    image_six   Longtext COMMENT '商品图片URL6',
    who_buy     char    COMMENT     '谁买了',
    PRIMARY KEY (ProductID),
    FOREIGN KEY (OwnerID) REFERENCES userinfo (UID)
) COMMENT ='二手交易市场的商品表';

INSERT INTO Products
(ProductName, OwnerID, Price, ImageURL, Description, `Condition`, Category, PostedDate, IsSold, IsGroom)
VALUES ('书桌', 1, 500.00, 'http://example.com/images/desk.jpg', '这是一个很好的书桌，几乎全新。', '七成新', '家具',
        '2024-04-21', FALSE, TRUE),
       ('电子书阅读器', 1, 1200.00, 'http://example.com/images/ebook_reader.jpg', '电子书阅读器，适合阅读各种电子书。',
        '九成新', '电子产品', '2024-04-22', FALSE, TRUE),
       ('编程书籍', 1, 100.00, 'http://example.com/images/programming_book.jpg',
        '这是一本关于Python编程的书籍，适合初学者。', '九成新', '书籍', '2024-04-23', FALSE, TRUE),
       ('沙发', 1, 800.00, 'http://example.com/images/sofa.jpg', '这是一张舒适的沙发，适合家庭使用。', '九成新',
        '家具', '2024-04-24', FALSE, TRUE),
       ('平板电脑', 1, 1500.00, 'http://example.com/images/tablet.jpg', '这是一台全新的平板电脑，性能强大。', '全新',
        '电子产品', '2024-04-25', TRUE, TRUE),
       ('小说书籍', 1, 50.00, 'http://example.com/images/novel_book.jpg', '这是一本畅销小说，情节扣人心弦。', '九成新',
        '书籍', '2024-04-26', FALSE, TRUE);
INSERT INTO Products
(ProductName, OwnerID, Price, ImageURL, Description, `Condition`, Category, PostedDate, IsSold, IsGroom)
VALUES ('书桌', 2, 500.00, 'http://example.com/images/desk.jpg', '这是一个很好的书桌，几乎全新。', '七成新', '家具',
        '2024-04-21', FALSE, TRUE);

-- 创建商品类别数据表
CREATE TABLE ProductCategories (
    CategoryID INT AUTO_INCREMENT PRIMARY KEY, -- 类别ID，自动递增，主键
    CategoryName VARCHAR(255) NOT NULL, -- 类别名称
    ParentID INT NULL, -- 父类别ID，用于构建类别层次结构，NULL表示顶级类别
    Description TEXT NULL, -- 类别描述
    CreationDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP, -- 类别创建时间
    LastModifiedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP -- 类别最后修改时间
);

-- 插入父类别数据
INSERT INTO ProductCategories (CategoryName) VALUES ('家具'); -- 插入家具类别
INSERT INTO ProductCategories (CategoryName) VALUES ('电子产品'); -- 插入电子产品类别
INSERT INTO ProductCategories (CategoryName) VALUES ('书籍'); -- 插入书籍类别
INSERT INTO ProductCategories (CategoryName) VALUES ('时尚服饰'); -- 插入时尚服饰类别
INSERT INTO ProductCategories (CategoryName) VALUES ('奢侈品'); -- 插入奢侈品类别
INSERT INTO ProductCategories (CategoryName) VALUES ('美妆个护'); -- 插入美妆个护类别
INSERT INTO ProductCategories (CategoryName) VALUES ('家用电器'); -- 插入家用电器类别
INSERT INTO ProductCategories (CategoryName) VALUES ('玩具与游戏'); -- 插入玩具与游戏类别
INSERT INTO ProductCategories (CategoryName) VALUES ('运动健身器材'); -- 插入运动健身器材类别
INSERT INTO ProductCategories (CategoryName) VALUES ('音乐与影视'); -- 插入音乐与影视类别
INSERT INTO ProductCategories (CategoryName) VALUES ('艺术与收藏品'); -- 插入艺术与收藏品类别
INSERT INTO ProductCategories (CategoryName) VALUES ('手工艺品'); -- 插入手工艺品类别
INSERT INTO ProductCategories (CategoryName) VALUES ('办公设备与文具'); -- 插入办公设备与文具类别
INSERT INTO ProductCategories (CategoryName) VALUES ('汽车配件'); -- 插入汽车配件类别

-- 查询插入的数据
SELECT * FROM ProductCategories; -- 查询并显示商品类别数据表中的所有记录


SELECT Products.ProductName, userinfo.username, Products.Price, Products.ImageURL, Products.Description, Products.Condition, Products.PostedDate, userinfo.Credits
            FROM Products
            INNER JOIN userinfo ON Products.OwnerID = userinfo.UID
            WHERE Products.ProductID = 1297;


create table Collection
(
    username   char    null,
    ID         int     null
);

create table cart
(
    username char(255) null,
    ID       int       null
);

create table chat
(
    user1     char(255) null,
    user2     char(255) null,
    message1  longtext  null,
    message2  longtext  null,
    chat_time datetime  null
);

alter table userinfo
    modify phone bigint null;

