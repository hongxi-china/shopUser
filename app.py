from flask import Flask, render_template, request, jsonify

from flask_cors import CORS
from api.loginLogon import Login_Logon, get_db_connection  # 确保正确导入Login_Logon
from api.user.someApi import Picture  # 确保正确导入Picture
from api.user.chat import Chat  # 确保正确导入Picture
from api.user.buy import Buy
from api.user.shipments import Shipments
from functools import wraps
from flask import request, redirect
import jwt
from waitress import serve



app = Flask(__name__)
CORS(app)

# 实例化Login_Logon类，并传递app对象
login_logon = Login_Logon(app)

Picture = Picture(app)
Chat = Chat(app)
Buy = Buy(app)
Shipments = Shipments(app)


@app.route('/')
@app.route('/home')
def html():
    return render_template('html/home.html')


@app.route('/login')
def home():
    return render_template('html/login.html')


@app.route('/register')
def register():
    return render_template('html/register.html')


@app.route('/myself')
def myself():
    return render_template('html/myself.html')


@app.route('/myshell')
def myshell():
    return render_template('html/myshell.html')


@app.route('/upload')
def upload():
    return render_template('html/upload.html')


@app.route('/show')
def show():
    searchText = request.args.get('searchText', '')
    selectedCategory = request.args.get('selectedCategory', '')
    return render_template('html/show_select.html', searchText=searchText, selectedCategory=selectedCategory)


@app.route('/shopping')
def shopping():
    return render_template('html/shopping.html')


@app.route('/details')
def details():
    product_id = request.args.get('id', '')
    return render_template('html/details.html', product_id=product_id)


@app.route('/chat')
def chat():
    currentUser = request.args.get('currentUser')
    sellerUser = request.args.get('sellerUser')
    return render_template('html/chat.html', currentUser=currentUser, sellerUser=sellerUser)


@app.route('/mycollection')
def mydetails():
    return (render_template('html/mycollection.html'))


@app.route('/mycart')
def mycart():
    return (render_template('html/mycart.html'))


@app.route('/revise')
def revise():
    product_id = request.args.get('id')  # 从查询参数中获取 id
    return render_template('html/revise.html', product_id=product_id)  # 将 id 作为一个变量传递给模板


@app.route('/mybooking')
def mybooking():
    return (render_template('html/mybooking.html'))


@app.route('/shipment')
def shipment():
    return (render_template('html/shipments.html'))

@app.route('/more')
def more():
    return (render_template('html/more.html'))

if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=5000)